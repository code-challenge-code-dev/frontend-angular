
export const environment = {
  baseUrl: 'http://localhost:8081/api/management',
  versions: {
    v1: {
      userAccounts: '/v1/user-accounts',
      userAccountsFilter: '/v1/user-accounts/filter',
      departments: '/v1/departments'
    }
  }
};
