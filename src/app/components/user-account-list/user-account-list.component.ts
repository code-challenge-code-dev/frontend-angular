import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {MatSort, MatSortModule} from '@angular/material/sort';
import {debounceTime, distinctUntilChanged, filter, merge, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatTableModule} from '@angular/material/table';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {DatePipe, NgForOf} from '@angular/common';
import {UserAccountService} from "../../services/user-account/user-account.service";
import {UserAccount, UserAccountFilterResponse} from "../../models/UserAccount";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {UserAccountDetailsModalComponent} from "../user-account-details-modal/user-account-details-modal.component";
import {UserAccountDeleteModalComponent} from "../user-account-delete-modal/user-account-delete-modal.component";
import {Router} from "@angular/router";
import {MatButton, MatIconButton} from "@angular/material/button";
import {MatFormField} from "@angular/material/form-field";
import {MatIcon} from "@angular/material/icon";
import {MatInput} from "@angular/material/input";
import {CustomPaginatorIntl} from "./custom-paginator-intl.service";
import {FormControl, ReactiveFormsModule} from "@angular/forms";
import {SnackbarService} from "../../services/snack-bar/snack-bar.service";

@Component({
  selector: 'user-account-list',
  styleUrl: 'user-account-list.component.css',
  templateUrl: 'user-account-list.component.html',
  standalone: true,
  imports: [
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    DatePipe,
    MatButton,
    MatFormField,
    MatIcon,
    MatInput,
    NgForOf,
    MatIconButton,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: CustomPaginatorIntl }
  ]
})
export class UserAccountListComponent implements AfterViewInit, OnInit {
  searchText: string | undefined;
  nameSearchControl = new FormControl();
  displayedColumns: string[] = ['name', 'email', 'department.name', 'actions'];
  data: UserAccount[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort) sort: MatSort | undefined;

  constructor(
    private router: Router,
    private userAccountService: UserAccountService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService
  ) {}

  ngAfterViewInit() {
    this.sort!.sortChange.subscribe(() => (this.paginator!.pageIndex = 0));

    merge(this.sort!.sortChange, this.paginator!.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;

          return this.userAccountService!.findByFilter(
            this.sort!.active,
            this.sort!.direction,
            this.paginator!.pageIndex,
            this.paginator!.pageSize,
            this.searchText
          ).pipe(catchError(() => observableOf(null)));
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;
          if (data === null)
            return [];

          this.resultsLength = data.total_count;
          return data.items;
        }),
      )
      .subscribe(data => (this.data = data));
  }

  private setupSearch() {
    this.nameSearchControl.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      filter((value: string) => value.length >= 3 || value.length === 0)
    ).subscribe(value => {
      this.searchText = value.trim().toLowerCase();

      const e = this.userAccountService!.findByFilter(
        this.sort!.active,
        this.sort!.direction,
        this.paginator!.pageIndex,
        this.paginator!.pageSize,
        this.searchText
      ).pipe(catchError(() => observableOf(null)))
        .subscribe({
          // @ts-ignore
          next: (data: UserAccountFilterResponse) => {
            this.resultsLength = data.total_count;
            this.data = data.items;

            this.isLoadingResults = false;
            this.isRateLimitReached = e === null;
            if (e === null) {
              return [];
            }
          },
          error: (error) => {
            this.snackbarService.openNotificationErrorSnackBar("Erro ao tentar carregar lista de usuários!");
          }
        });
      return e;
    });
  }

  ngOnInit(): void {
    this.setupSearch();
  }

  navigateToCreateUserAccount() {
    this.router.navigate(['/create-users']);
  }

  navigateToUpdateUserAccount(userAccount: UserAccount) {
    this.router.navigate([`/update-users/${userAccount.id}`]);
  }

  openUserAccountDetailsModal(user: UserAccount) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = user;
    dialogConfig.width = '80%';
    this.dialog.open(UserAccountDetailsModalComponent, dialogConfig);
  }

  openDeleteUserAccountModal(userAccount: UserAccount): void {
    let dialogRef = this.dialog.open(UserAccountDeleteModalComponent, {
      data: { id: userAccount.id },
      width: '80%',
      enterAnimationDuration: '0ms',
      exitAnimationDuration: '0ms',
    });

    const sub = dialogRef.componentInstance.onDelete.subscribe((uuid) => {
      this.data = this.data.filter((user: { id: string | undefined; }) => user.id !== uuid);
    });
  }

}
