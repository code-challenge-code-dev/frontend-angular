import {Component, OnInit} from '@angular/core';
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {UserAccount} from "../../models/UserAccount";
import {Department} from "../../models/Department";
import {UserAccountService} from "../../services/user-account/user-account.service";
import {DepartmentService} from "../../services/department/department.service";
import {MatButton} from "@angular/material/button";
import {MatError, MatFormField, MatLabel} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";
import {MatOption} from "@angular/material/autocomplete";
import {MatSelect} from "@angular/material/select";
import {SnackbarService} from "../../services/snack-bar/snack-bar.service";

@Component({
  selector: 'app-user-account-update',
  standalone: true,
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        MatButton,
        MatFormField,
        MatInput,
        MatLabel,
        MatOption,
        MatSelect,
        MatError
    ],
  templateUrl: './user-account-update.component.html',
  styleUrl: './user-account-update.component.scss'
})
export class UserAccountUpdateComponent implements OnInit {

  user: UserAccount = {
    id: '',
    name: '',
    email: '',
    department: {
      id: '',
      name: ''
    }
  };

  userId: string | null = '';
  departments: Department[] = [];

  constructor(
    private route: ActivatedRoute,
    private userAccountService: UserAccountService,
    private departmentService: DepartmentService,
    private router: Router,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit(): void {
    this.userId = this.route.snapshot.paramMap.get('id');

    this.userAccountService.loadUserAccountById(this.userId!).subscribe({
      next: (data) => {
        this.user = data;
      },
      error: (error) => {
        this.snackbarService.openNotificationErrorSnackBar("Erro ao tentar carregar usuário selecionando!");
      }
    });

    this.departmentService.getDepartments().subscribe(
      {
        next: (data) => {
          this.departments = data;
        },
        error: (error) => {
          this.snackbarService.openNotificationErrorSnackBar("Erro ao tentar carregar lista de departamentos!");
        }
    });
  }

  onSubmit(userForm: NgForm) {
    if (userForm.invalid) {
      this.snackbarService.openNotificationErrorSnackBar("Por favor, preencha todos os campos obrigatórios corretamente.");
      return;
    }

    this.userAccountService.updateUserAccount(this.userId!, this.user).subscribe({
        next: (data) => {
          this.snackbarService.openNotificationSuccessSnackBar("Usuário atualizado com sucesso!");
          this.router.navigate(['/list-users']);
        },
        error: (error) => {
          this.snackbarService.openNotificationErrorSnackBar("Erro ao tentar atualizar usuário!");
        }
    });
  }

  navigateToUserList() {
    this.router.navigate(['/list-users']);
  }

}
