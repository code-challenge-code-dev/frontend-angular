import {Component} from '@angular/core';

@Component({
  selector: 'home',
  standalone: true,
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  homeText: string = "Home, selecione uma opção do menu lateral para iniciar.";
}
