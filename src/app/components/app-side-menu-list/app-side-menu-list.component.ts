import {Component} from '@angular/core';
import {
  MatList,
  MatListItem,
  MatListOption,
  MatListSubheaderCssMatStyler,
  MatSelectionList
} from "@angular/material/list";
import {Router} from "@angular/router";
import {MatOption} from "@angular/material/autocomplete";
import {MatButton, MatFabButton, MatIconButton} from "@angular/material/button";
import {MatIcon} from "@angular/material/icon";
import {MatDivider} from "@angular/material/divider";

@Component({
  selector: 'app-app-side-menu-list',
  standalone: true,
  imports: [
    MatSelectionList,
    MatListOption,
    MatOption,
    MatList,
    MatListItem,
    MatIconButton,
    MatIcon,
    MatFabButton,
    MatButton,
    MatDivider,
    MatListSubheaderCssMatStyler
  ],
  templateUrl: './app-side-menu-list.component.html',
  styleUrl: './app-side-menu-list.component.scss'
})
export class AppSideMenuListComponent {

  constructor(
    private router: Router
  ) { }

  menuListItem: MenuItem[] = [
    {
      name: 'Listar Usuários',
      path: '/list-users'
    },
    {
      name: 'Criar Novo Usuário',
      path: '/create-users'
    }
  ];

  navigationTo(path: string) {
    this.router.navigate([path]);
  }

}

export interface MenuItem {
  name: string;
  path: string;
}
