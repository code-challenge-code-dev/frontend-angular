import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AppSideMenuListComponent} from './app-side-menu-list.component';
import {Router} from "@angular/router";
import { By } from '@angular/platform-browser';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('AppSideMenuListComponent', () => {
  let component: AppSideMenuListComponent;
  let fixture: ComponentFixture<AppSideMenuListComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppSideMenuListComponent, NoopAnimationsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppSideMenuListComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render menu items', () => {
    const menuItems = fixture.debugElement.queryAll(By.css('mat-list-item'));
    expect(menuItems.length).toBe(2);
    expect(menuItems[0].nativeElement.textContent).toContain('Listar Usuários');
    expect(menuItems[1].nativeElement.textContent).toContain('Criar Novo Usuário');
  });

  it('should navigate to the correct path on menu item click', () => {
    const navigateSpy = spyOn(router, 'navigate');
    const menuItems = fixture.debugElement.queryAll(By.css('mat-list-item'));

    menuItems[0].nativeElement.click();
    expect(navigateSpy).toHaveBeenCalledWith(['/list-users']);

    menuItems[1].nativeElement.click();
    expect(navigateSpy).toHaveBeenCalledWith(['/create-users']);
  });

});
