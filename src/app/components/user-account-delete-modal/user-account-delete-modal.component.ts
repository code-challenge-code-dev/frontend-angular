import {ChangeDetectionStrategy, Component, EventEmitter, Inject, inject} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle,
} from '@angular/material/dialog';
import {UserAccountService} from "../../services/user-account/user-account.service";
import {Router} from "@angular/router";
import {SnackbarService} from "../../services/snack-bar/snack-bar.service";

@Component({
  selector: 'user-account-delete-modal',
  templateUrl: 'user-account-delete-modal.component.html',
  standalone: true,
  imports: [
    MatButtonModule,
    MatDialogActions,
    MatDialogClose,
    MatDialogTitle,
    MatDialogContent
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [UserAccountService],
})
export class UserAccountDeleteModalComponent {

  onDelete = new EventEmitter<string>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userAccountService: UserAccountService,
    private snackbarService: SnackbarService
  ) {}

  readonly dialogRef = inject(MatDialogRef<UserAccountDeleteModalComponent>);

  deleteConfirmation(element: any) {
    this.userAccountService.deleteUserAccountById(this.data.id!).subscribe({
      next: (data) => {
        this.onDelete.emit(element.id);
        this.snackbarService.openNotificationSuccessSnackBar("Usuário deletado com sucesso!")
      },
      error: err => {
        this.snackbarService.openNotificationErrorSnackBar(
          "Algo deu errado ao tentar deletar o usuário, contate o administrador!"
        )
      }
    });
  }

}
