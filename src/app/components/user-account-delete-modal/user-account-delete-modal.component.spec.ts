import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UserAccountDeleteModalComponent} from './user-account-delete-modal.component';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('UserAccountDeleteModalComponent', () => {
  let component: UserAccountDeleteModalComponent;
  let fixture: ComponentFixture<UserAccountDeleteModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        UserAccountDeleteModalComponent,
        NoopAnimationsModule,
        MatDialogModule,
        HttpClientModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserAccountDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
