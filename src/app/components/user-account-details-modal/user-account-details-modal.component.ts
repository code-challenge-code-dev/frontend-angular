import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {UserAccount} from "../../models/UserAccount";

@Component({
  selector: 'app-user-account-details-modal',
  templateUrl: './user-account-details-modal.component.html',
  standalone: true,
  styleUrl: './user-account-details-modal.component.scss'
})
export class UserAccountDetailsModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public user: UserAccount) {}
}
