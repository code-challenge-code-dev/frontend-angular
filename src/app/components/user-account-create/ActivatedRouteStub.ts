import { convertToParamMap, ParamMap, Params } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import {map} from "rxjs/operators";

export class ActivatedRouteStub {
  private subject = new ReplaySubject<Params>();

  constructor(initialParams?: Params) {
    if (initialParams) {
      this.setParams(initialParams);
    }
  }

  setParams(params: Params) {
    this.subject.next(params);
  }

  get snapshot(): any {
    return {
      paramMap: convertToParamMap(this.subject.asObservable())
    };
  }

  get paramMap() {
    return this.subject.asObservable().pipe(map(params => convertToParamMap(params)));
  }
}
