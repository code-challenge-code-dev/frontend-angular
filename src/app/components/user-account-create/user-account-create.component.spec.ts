import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UserAccountCreateComponent} from './user-account-create.component';
import {ActivatedRoute, Router} from "@angular/router";
import {DepartmentService} from "../../services/department/department.service";
import { of } from 'rxjs';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {UserAccountService} from "../../services/user-account/user-account.service";
import {SnackbarService} from "../../services/snack-bar/snack-bar.service";
import {ActivatedRouteStub} from "./ActivatedRouteStub";

describe('UserAccountCreateComponent', () => {
  let component: UserAccountCreateComponent;
  let fixture: ComponentFixture<UserAccountCreateComponent>;
  let router: Router;
  let userAccountService: UserAccountService;
  let departmentService: DepartmentService;
  let snackbarService: SnackbarService;
  let activatedRouteStub: ActivatedRouteStub;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        UserAccountCreateComponent,
        NoopAnimationsModule,
      ],
      providers: [
        UserAccountService,
        DepartmentService,
        SnackbarService,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccountCreateComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    userAccountService = TestBed.inject(UserAccountService);
    departmentService = TestBed.inject(DepartmentService);
    snackbarService = TestBed.inject(SnackbarService);
    activatedRouteStub = new ActivatedRouteStub({ id: '123' });

    spyOn(departmentService, 'getDepartments').and.returnValue(of([
      { id: '1', name: 'Department 1' },
      { id: '2', name: 'Department 2' }
    ]));

    spyOn(userAccountService, 'createUserAccount').and.returnValue(of({
      id: '123',
      name: 'Test User',
      email: 'test@example.com',
      department: { id: '1', name: 'Department 1' }
    }));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render form fields correctly', () => {
    const nameInput = fixture.debugElement.nativeElement.querySelector('input[name="name"]');
    const emailInput = fixture.debugElement.nativeElement.querySelector('input[name="email"]');
    const departmentSelect = fixture.debugElement.nativeElement.querySelector('mat-select[name="department"]');

    expect(nameInput).toBeTruthy();
    expect(emailInput).toBeTruthy();
    expect(departmentSelect).toBeTruthy();
  });

  it('should navigate to user list on back button click', () => {
    const navigateSpy = spyOn(router, 'navigate');
    const backButton = fixture.debugElement.nativeElement.querySelector('button');

    backButton.click();
    expect(navigateSpy).toHaveBeenCalledWith(['/list-users']);
  });

  // it('should show success snackbar on form submission', () => {
  //   const snackbarSpy = spyOn(snackbarService, 'openNotificationSuccessSnackBar');
  //   component.user.name = 'Test User';
  //   component.user.email = 'test@example.com';
  //   component.user.department.id = '1';
  //
  //   const form = fixture.debugElement.nativeElement.querySelector('form');
  //
  //   fixture.detectChanges();
  //   form.dispatchEvent(new Event('submit'));
  //
  //   expect(snackbarSpy).toHaveBeenCalledWith('Nova conta de usuário criada com sucesso.');
  // });

  // it('should show error snackbar on form submission error', () => {
  //   spyOn(userAccountService, 'createUserAccount').and.returnValue(of({
  //     error: 'Error creating user'
  //   }));
  //   const snackbarSpy = spyOn(snackbarService, 'openNotificationErrorSnackBar');
  //
  //   component.user.name = 'Test User';
  //   component.user.email = 'test@example.com';
  //   component.user.department.id = '1';
  //
  //   const form = fixture.debugElement.nativeElement.querySelector('form');
  //   form.dispatchEvent(new Event('submit'));
  //
  //   fixture.detectChanges();
  //
  //   expect(snackbarSpy).toHaveBeenCalledWith('Erro ao tentar criar novo usuário, por favor, contate o administrador!');
  // });

});
