import {Component, OnInit} from '@angular/core';
import {UserAccountService} from '../../services/user-account/user-account.service';
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, NgForm, ReactiveFormsModule} from "@angular/forms";
import {DepartmentService} from "../../services/department/department.service";
import {Router} from "@angular/router";
import {Department} from "../../models/Department";
import {MatError, MatFormField, MatLabel} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";
import {MatIcon} from "@angular/material/icon";
import {MatButton, MatIconButton} from "@angular/material/button";
import {MatListItem} from "@angular/material/list";
import {MatOption, MatSelect} from "@angular/material/select";
import {MatDivider} from "@angular/material/divider";
import {SnackbarService} from "../../services/snack-bar/snack-bar.service";

@Component({
  selector: 'app-user-account-create',
  standalone: true,
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    MatFormField,
    MatInput,
    ReactiveFormsModule,
    MatIcon,
    MatIconButton,
    MatButton,
    MatListItem,
    MatSelect,
    MatOption,
    MatSelect,
    MatLabel,
    MatDivider,
    MatError
  ],
  templateUrl: './user-account-create.component.html',
  styleUrl: './user-account-create.component.scss'
})
export class UserAccountCreateComponent implements OnInit {

  user = {
    id: '',
    name: '',
    email: '',
    department: {
      id: '',
      name: ''
    }
  };

  departments: Department[] = [];

  constructor(
    private router: Router,
    private userAccountService: UserAccountService,
    private departmentService: DepartmentService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit(): void {
    this.departmentService.getDepartments().subscribe(
      {
        next: (data) => {
          this.departments = data;
        },
        error: (error) => {
          this.snackbarService.openNotificationErrorSnackBar("Erro ao tentar carregar lista de departamentos!");
        }
    });
  }

  onSubmit(userForm: NgForm) {

    if (userForm.invalid) {
      this.snackbarService.openNotificationErrorSnackBar("Por favor, preencha todos os campos obrigatórios corretamente.");
      return;
    }

    this.userAccountService.createUserAccount(this.user).subscribe({
        next: (data) => {
          this.snackbarService.openNotificationSuccessSnackBar("Nova conta de usuário criada com sucesso.");
          this.router.navigate(['/list-users']);
        },
        error: (error) => {
          this.snackbarService.openNotificationErrorSnackBar("Erro ao tentar criar novo usuário, por favor, contate o administrador!");
        }
    });

  }

  navigateToUserList() {
    this.router.navigate(['/list-users']);
  }

}
