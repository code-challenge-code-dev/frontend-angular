import {Routes} from '@angular/router';
import {UserAccountListComponent} from './components/user-account-list/user-account-list.component';
import {UserAccountCreateComponent} from './components/user-account-create/user-account-create.component';
import {UserAccountUpdateComponent} from "./components/user-account-udpate/user-account-update.component";
import {HomeComponent} from "./components/home/home.component";

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'list-users', component: UserAccountListComponent },
  { path: 'create-users', component: UserAccountCreateComponent },
  { path: 'update-users/:id', component: UserAccountUpdateComponent }
];
