import {Component} from '@angular/core';
import {Router, RouterOutlet} from '@angular/router';
import {UserAccountListComponent} from "./components/user-account-list/user-account-list.component";
import {UserAccountCreateComponent} from "./components/user-account-create/user-account-create.component";
import {CommonModule} from "@angular/common";
import {MatToolbar} from "@angular/material/toolbar";
import {MatIcon} from "@angular/material/icon";
import {MatCard, MatCardActions, MatCardContent, MatCardHeader} from "@angular/material/card";
import {MatDrawer, MatDrawerContainer} from "@angular/material/sidenav";
import {MatDivider} from "@angular/material/divider";
import {AppSideMenuListComponent} from "./components/app-side-menu-list/app-side-menu-list.component";
import {MatIconButton} from "@angular/material/button";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    UserAccountListComponent,
    UserAccountCreateComponent,
    CommonModule,
    MatToolbar,
    MatIcon,
    MatCardHeader,
    MatCard,
    MatCardContent,
    MatCardActions,
    MatDrawerContainer,
    MatDrawer,
    MatDivider,
    AppSideMenuListComponent,
    MatIconButton,
    UserAccountListComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {

  constructor(
    private router: Router
  ) { }

  navigateToListUserAccount() {
    this.router.navigate(['/list-users']);
  }

}
