import {Department} from "./Department";

export interface UserAccount {
  id: string | undefined;
  name: string;
  email: string;
  department: Department;
  // createdAt: string;
}

export interface UserAccountFilterResponse {
  items: UserAccount[];
  total_count: number;
}
