import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Department} from "../../models/Department";
import {environment} from "../../api-endpoints";

@Injectable({
  providedIn: 'any'
})
export class DepartmentService {

  constructor(private http: HttpClient) { }

  getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(`${environment.baseUrl}${environment.versions.v1.departments}`);
  }

}
