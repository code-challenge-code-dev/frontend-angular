import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserAccount, UserAccountFilterResponse} from "../../models/UserAccount";
import {SortDirection} from "@angular/material/sort";
import {environment} from "../../api-endpoints";

@Injectable({
  providedIn: 'any'
})
export class UserAccountService {

  headers = new HttpHeaders({
    'Accept-Language': "en"
  });

  constructor(public http: HttpClient) { }

  loadUserAccountById(uuid: string): Observable<UserAccount> {
    return this.http.get<UserAccount>(
      `${environment.baseUrl}${environment.versions.v1.userAccounts}/${uuid}`,
      { headers: this.headers }
    );
  }

  createUserAccount(userAccount: UserAccount): Observable<UserAccount> {
    return this.http.post<UserAccount>(
      `${environment.baseUrl}${environment.versions.v1.userAccounts}`,
      userAccount
    );
  }

  updateUserAccount(uuid: string, userAccount: UserAccount): Observable<UserAccount> {
    return this.http.put<UserAccount>(
      `${environment.baseUrl}${environment.versions.v1.userAccounts}/${uuid}`,
      userAccount
    );
  }

  deleteUserAccountById(uuid: string): Observable<UserAccount> {
    return this.http.delete<UserAccount>(
      `${environment.baseUrl}${environment.versions.v1.userAccounts}/${uuid}`,
      { headers: this.headers }
    );
  }

  findByFilter(
    sort: string,
    order: SortDirection,
    page: number,
    size: number,
    searchText?: string
  ): Observable<UserAccountFilterResponse> {
    let requestUrl;
    if (searchText !== undefined)
      requestUrl = `${environment.baseUrl}${environment.versions.v1.userAccountsFilter}?sort=${sort}&order=${order}&page=${page}&size=${size}&searchText=${searchText}`;
    else
      requestUrl = `${environment.baseUrl}${environment.versions.v1.userAccountsFilter}?sort=${sort}&order=${order}&page=${page}&size=${size}`;

    return this.http.get<UserAccountFilterResponse>(requestUrl);
  }

}
